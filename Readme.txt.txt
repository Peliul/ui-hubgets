Environment:

Project requires: 
	1. NodeJS: v12.14.1
	2. Angular CLI: v8.3.26

Working environment: 
	- Visual Studio Code

To run the project you need to:
	1. Open command line interface and go to project location
	2. Type 'npm install'
	3. Type 'ng serve'
	4. Navigate in your browser at localhost:4200
